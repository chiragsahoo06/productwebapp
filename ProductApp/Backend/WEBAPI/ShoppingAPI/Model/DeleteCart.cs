﻿namespace ShoppingAPI.Model
{
    public class DeleteCart
    {
        public string Username { get; set; }
        public int ProductId { get; set; }
    }
}
