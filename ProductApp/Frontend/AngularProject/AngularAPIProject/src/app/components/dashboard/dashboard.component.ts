import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  products?:Product[]
  Username?:string

  constructor(private productService:ProductService, private router:ActivatedRoute) {
    console.log("This is dashboard Constructor");
    this.router.params.forEach(data => {
      this.Username = data['Username']
      console.log(this.Username);
     })

   }

  ngOnInit(): void {
    console.log("This is OnItMethod");
    this.productService.getAllProducts().subscribe(res=>{
      console.log(res);
      this.products=res;
    });
       }
}
